package com.substructio25.springdemo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class DemoController {
    @RequestMapping("/test")
    public String getTest(){
        return "Test test";
    }
}
